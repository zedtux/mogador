FROM ruby:2.7-alpine3.12
WORKDIR /gem

# `deps` installs all the system libraries required by the gem in order
# to install all its dependencies and then installs them.
# Finally it creates a temporary image used as a base image later.
deps:
    ARG BUILD_PACKAGES
    ENV BUILD_PACKAGES=${BUILD_PACKAGES:-git}

    RUN echo "BUILD_PACKAGES: $BUILD_PACKAGES"

    COPY lib/mogador/version.rb /gem/lib/mogador/version.rb
    COPY Gemfile* *.gemspec README.md /gem/

    RUN apk add --no-cache $BUILD_PACKAGES \
        && touch ~/.gemrc \
        && echo "gem: --no-ri --no-rdoc" >> ~/.gemrc \
        && gem install rubygems-update \
        && update_rubygems \
        && gem install bundler \
        && bundle install --jobs $(nproc) \
        && rm -rf /usr/local/bundle/cache/*.gem \
        && find /usr/local/bundle/gems/ -name "*.c" -delete \
        && find /usr/local/bundle/gems/ -name "*.o" -delete

# `build` builds the mogador-<version>.gem file
build:
    FROM +deps

    COPY . /gem

    RUN gem build mogador.gemspec \
        && gem install mogador-*.gem \
        && mogador \
        && mogador templates

# `docker` builds the final gem Docker image.
docker:
    FROM +build

    ENTRYPOINT ["bundle", "exec"]
    CMD ["rake"]

    SAVE IMAGE registry.gitlab.com/zedtux/mogador:latest

# `tests` runs the RSpec test suite.
tests:
    FROM +deps

    ARG CMD=rake

    COPY . /gem

    RUN bundle exec $CMD
