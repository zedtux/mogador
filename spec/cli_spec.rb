# frozen_string_literal: true

RSpec.describe Mogador::Cli do
  context 'when passing "help"' do
    let(:args) { ['help'] }
    let(:output) { capture(:stdout) { described_class.start(args) } }

    it "prints a help page including the 'version' command" do
      expect(output).to match(/version\s+# Show the version and exist/)
    end

    it "prints a help page including the 'exec' command" do
      expect(output).to match(
        /exec COMMAND\s+# Execute the given command in the base.../
      )
    end

    it "prints a help page including the 'templates' command" do
      expect(output).to match(
        /templates SUBCOMMAND ...ARGS\s+# Manage the Mogador templates/
      )
    end
  end

  context 'when passing "templates"' do
    let(:args) { ['templates'] }
    let(:output) { capture(:stdout) { described_class.start(args) } }

    it "prints a help page including the 'templates install' command" do
      expect(output).to match(
        /templates install TEMPLATE_NAME\s+# Installs the given template name/
      )
    end

    it "prints a help page including the 'templates list' command" do
      expect(output).to match(
        /templates list\s+# List all the available templates/
      )
    end

    it "prints a help page including the 'templates use' command" do
      expect(output).to match(
        /templates use TEMPLATE_NAME\s+# Creates\/updates the .mogador.yml f.../
      )
    end
  end
end
