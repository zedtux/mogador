# frozen_string_literal: true

RSpec.describe Mogador do
  let(:current_path) { Pathname.new(Dir.pwd) }

  before { Mogador.config = nil }

  describe 'app_root' do
    it 'sets the app_root to the current path' do
      expect(Mogador.app_root).to eql current_path
    end

    it 'sets the base_root to /usr/local/mogador' do
      expect(Mogador.base_root).to eql Pathname.new('/usr/local/mogador')
    end
  end

  describe 'base_root' do
    it 'sets the app_root to the current path' do
      expect(Mogador.app_root).to eql current_path
    end

    it 'sets the base_root to /usr/local/mogador' do
      expect(Mogador.base_root).to eql Pathname.new('/usr/local/mogador')
    end
  end

  context 'when configure has not been called and trying to access '\
          'configuration' do
    it 'raises a Mogador::Errors::NotInitializedError' do
      expect { Mogador.configuration }.to change {
        Mogador.config
      }.from(nil).to(Mogador::Configuration)
    end
  end
end

RSpec.describe Mogador::Configuration do
  it 'responds to app_root' do
    expect(described_class.new).to respond_to(:app_root)
  end

  it 'responds to base_root' do
    expect(described_class.new).to respond_to(:base_root)
  end

  it 'responds to templates_cache_path' do
    expect(described_class.new).to respond_to(:templates_cache_path)
  end

  it 'responds to templates_cache_catalog_path' do
    expect(described_class.new).to respond_to(:templates_cache_catalog_path)
  end

  describe 'initialize' do
    it 'sets the app_root to nil' do
      expect(described_class.new.app_root).to be_nil
    end

    it 'sets the base_root to /usr/local/mogador' do
      expect(described_class.new.base_root)
        .to eql Pathname.new('/usr/local/mogador')
    end

    it 'sets the templates_cache_path to /usr/local/mogador/templates_cache' do
      expect(
        described_class.new.templates_cache_path
      ).to eql Pathname.new('/usr/local/mogador/templates_cache')
    end

    it 'sets the templates_cache_catalog_path to ' \
       '/usr/local/mogador/templates_cache/catalog.yml' do
      expect(
        described_class.new.templates_cache_catalog_path
      ).to eql Pathname.new('/usr/local/mogador/templates_cache/catalog.yml')
    end
  end

  describe 'app_root=' do
    context 'when passing a path' do
      let(:new_app_root) { '/test/path' }

      before do
        @configuration = described_class.new
        @configuration.app_root = new_app_root
      end

      it 'saves it as a Pathname' do
        expect(@configuration.app_root).to eql Pathname.new(new_app_root)
      end
    end
  end

  describe 'base_root=' do
    context 'when passing a path' do
      let(:new_base_root) { '/test/path' }

      before do
        @configuration = described_class.new
        @configuration.base_root = new_base_root
      end

      it 'saves it as a Pathname' do
        expect(@configuration.base_root).to eql Pathname.new(new_base_root)
      end

      it 'updates the templates_cache_path from the base_root' do
        expect(
          @configuration.templates_cache_path
        ).to eql Pathname.new(new_base_root).join('templates_cache')
      end

      it 'updates the templates_cache_catalog_path from the base_root' do
        expect(
          @configuration.templates_cache_catalog_path
        ).to eql Pathname.new(new_base_root).join('templates_cache',
                                                  'catalog.yml')
      end
    end
  end
end
