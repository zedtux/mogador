# frozen_string_literal: true

require 'webmock/rspec'

require 'bundler/setup'
require 'mogador'
require 'mogador/cli'
require 'mogador/configuration'

require 'support/capture_helper'
require 'support/webmock_helpers'

ENV['RUBY_ENV'] = 'test'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.include CaptureHelper
  config.include WebmockHelpers
end
