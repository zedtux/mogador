# frozen_string_literal: true

RSpec.describe Mogador::Cli do
  context 'when passing "templates use"' do
    before { stub_the_request(to: :mogador_templates) }
    before { stub_the_request(to: :mogador_templates_tree) }
    before { stub_the_request(to: :mogador_rails_5_template_download) }

    context "when there's no installed templates" do
      let(:args) { %w[templates use mytemplate] }

      it 'raises a Mogador::Errors::NoInstalledTemplateError' do
        expect { described_class.start(args) }.to raise_error(
          Mogador::Errors::NoInstalledTemplateError,
          "ERROR: There's no installed templates yet."
        )
      end
    end

    context 'when passing a template which is not yet installed' do
      let(:args) { %w[templates use mytemplate] }

      before { described_class.start(%w[templates install rails-5-base]) }

      it 'raises a Mogador::Errors::NoInstalledTemplateError' do
        expect { described_class.start(args) }.to raise_error(
          Mogador::Errors::NoInstalledTemplateError,
          "ERROR: There's no installed templates yet."
        )
      end
    end

    context 'when passing a template which is installed' do
      let(:args) { %w[templates use rails-5-base] }
      let(:mogador_yml) { Pathname.new(File.join(Dir.pwd, '.mogador.yml')) }

      before { described_class.start(%w[templates install rails-5-base]) }

      it 'raises not any errors' do
        expect { described_class.start(args) }.to_not raise_error
      end

      it 'creates a .mogador.yml file' do
        expect(mogador_yml).to exist
        expect(mogador_yml).to be_file
      end

      it 'creates a .mogador.yml file which define a template_path' do
        expect(YAML.load_file(mogador_yml)).to have_key('template_path')
        expect(YAML.load_file(mogador_yml)['template_path'])
          .to eql '/usr/local/mogador/templates_cache/rails-5-base'
      end
    end
  end
end
