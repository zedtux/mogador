# frozen_string_literal: true

RSpec.describe Mogador::Cli do
  context 'when passing "templates install"' do
    before { stub_the_request(to: :mogador_templates) }
    before { stub_the_request(to: :mogador_templates_tree) }

    context "when the mogador-template doesn't exist" do
      let(:args) { %w[templates install idonoexists] }

      it 'raises a Mogador::Errors::TemplateInstallError with \"The template ' \
         "idonoexists doesn't exist\" message" do
        expect { described_class.start(args) }.to raise_error(
          Mogador::Errors::TemplateInstallError,
          "The template \"idonoexists\" doesn't exist"
        )
      end
    end

    context 'when the mogador-template exists' do
      before { stub_the_request(to: :mogador_rails_5_template_download) }

      let(:args) { %w[templates install rails-5-base] }
      let(:templates_cache_path) { Mogador.base_root.join('templates_cache') }
      let(:template_tar_gz_path) do
        Pathname.new(File.join(templates_cache_path, 'rails-5-base.tar.gz'))
      end
      let(:template_folder_path) do
        Pathname.new(File.join(templates_cache_path, 'rails-5-base'))
      end

      it 'downloads the template as a tar.gz file' do
        described_class.start(args)

        expect(template_tar_gz_path).to exist
        expect(template_tar_gz_path).to be_file
      end

      it 'extracts the template' do
        described_class.start(args)

        expect(template_folder_path).to exist
        expect(template_folder_path).to be_directory
      end
    end
  end
end
