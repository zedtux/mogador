# frozen_string_literal: true

RSpec.describe Mogador::Cli do
  context 'when passing "exec"' do
    let(:args) { ['exec'] }
    let(:original_dir_pwd) { Dir.pwd }
    let(:test_dir) { File.join(original_dir_pwd, 'tmp', 'cli-tests') }

    before do
      stub_the_request(to: :mogador_templates)
      stub_the_request(to: :mogador_templates_tree)
      stub_the_request(to: :mogador_rails_5_template_download)

      FileUtils.rm_rf(test_dir) && FileUtils.mkdir_p(test_dir)
    end

    it 'sets the Mogador configuration app_root to the current dir' do
      allow(Dir).to receive(:pwd).and_return(test_dir)

      described_class.start(args)

      expect(Mogador.app_root).to eql Pathname.new(test_dir)
    end

    it "prints a 'missing command' error message" do
      expect(
        capture(:stdout) { described_class.start(args) }
      ).to include 'You must pass a command to be executed'
    end

    context 'with a command' do
      let(:args) { %w[exec whoami] }

      context 'without a .mogador.yml file' do
        let(:stdout) { capture(:stdout) { described_class.start(args) } }
        it 'prints a "Mogador not configured, please use ' \
           '`mogador templates use` command" error' do
          expect(stdout).to include 'Mogador not configured, please use ' \
                                    '`mogador templates use` command'
        end
      end

      context 'with a .mogador.yml file' do
        before do
          described_class.start %w[templates install rails-5-base]
          described_class.start %w[templates use rails-5-base]
        end

        it "sets the Mogador base_root to the template's path" do
          described_class.start(args)

          expect(Mogador.base_root)
            .to eql Pathname.new('/usr/local/mogador/templates_cache/rails-5-base')
        end

        it 'runs the command in the base_root' do
          expect(Dir).to receive(:chdir).with(Mogador.base_root)

          described_class.start(args)
        end

        it 'runs the command using the Kernel.system method' do
          expect_any_instance_of(Kernel).to receive(:system).with('whoami')

          described_class.start(args)
        end
      end
    end
  end
end
