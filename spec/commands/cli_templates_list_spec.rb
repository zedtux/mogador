# frozen_string_literal: true

RSpec.describe Mogador::Cli do
  context 'when passing "templates list"' do
    let(:args) { %w[templates list] }

    context "when the mogador-templates repository doesn't exist" do
      before { stub_the_request(to: :mogador_templates, with_status: 404) }

      it "raises a Mogador::Errors::TemplateFetchingError with 'Something " \
         "went wrong, mogador-templates project cannot be found' message" do
        expect { described_class.start(args) }.to raise_error(
          Mogador::Errors::TemplateFetchingError,
          'Something went wrong, mogador-templates project cannot be found'
        )
      end
    end

    context 'when the mogador-templates repository exists' do
      before { stub_the_request(to: :mogador_templates) }

      context 'without any template folders' do
        before do
          stub_the_request(to: :mogador_templates_tree, with_status: 404)
        end

        it 'raises a Mogador::Errors::TemplateFetchingError with ' \
           "'The mogador-templates project has no templates yet' message" do
          expect { described_class.start(args) }.to raise_error(
            Mogador::Errors::TemplateFetchingError,
            'The mogador-templates project has no templates yet'
          )
        end
      end

      context 'with template folders' do
        before { stub_the_request(to: :mogador_templates_tree) }

        let(:output) { capture(:stdout) { described_class.start(args) } }

        it 'prints the template list including the rails-5-base template' do
          expect(output).to include 'rails-5-base'
        end

        it 'prints the template list including the rails-6-base template' do
          expect(output).to include 'rails-6-base'
        end
      end
    end
  end
end
