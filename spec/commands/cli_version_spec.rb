# frozen_string_literal: true

RSpec.describe Mogador::Cli do
  context 'when passing "version"' do
    let(:args) { ['version'] }
    let(:output) { capture(:stdout) { described_class.start(args) } }

    it "prints the Mogador's version" do
      expect(output).to include Mogador::VERSION
    end
  end
end
