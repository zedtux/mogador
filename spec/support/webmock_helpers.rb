# frozen_string_literal: true

module WebmockHelpers
  class UnknownStubDestination < StandardError; end

  def stub_the_request(options = {})
    raise ArgumentError, 'Missing the "to" option' unless options.key?(:to)

    options[:with_status] ||= 200

    request_verb_and_url = request_verb_and_url_for(options[:to])

    stub_request(
      *request_verb_and_url
    ).to_return(
      status: options[:with_status],
      body: response_body_for(options[:to], options[:with_status])
    )
  end

  def request_verb_and_url_for(to)
    case to
    when :mogador_templates
      [:get, 'https://gitlab.com/api/v4/projects/zedtux%2Fmogador-templates']
    when :mogador_templates_tree
      [:get, 'https://gitlab.com/api/v4/projects/22064659/repository/tree']
    when :mogador_rails_5_template_download
      [:get, 'https://gitlab.com/zedtux/mogador-templates/-/archive/master/mogador-templates-master.tar.gz?path=rails-5-base']
    else
      raise UnknownStubDestination, "The #{to} stub destination is unknown"
    end
  end

  def response_body_for(to, with_status)
    case to
    when :mogador_templates
      if with_status == 200
        gitlab_mogador_templates_project_json
      elsif with_status == 404
        not_found_json
      else
        raise UnknownStatus, "With status #{with_status} is not managed for " \
                             "the #{to} destination."
      end
    when :mogador_templates_tree
      if with_status == 200
        gitlab_mogador_templates_project_tree_json
      elsif with_status == 404
        not_found_json
      else
        raise UnknownStatus, "With status #{with_status} is not managed for " \
                             "the #{to} destination."
      end
    when :mogador_rails_5_template_download
      File.read(
        File.join('spec', 'fixtures', 'templates', 'rails-5-base.tar.gz')
      )
    else
      raise UnknownStubDestination, "The #{to} stub destination is unknown"
    end
  end

  def gitlab_mogador_templates_project_json
    {
      id: '22064659',
      description: 'This repository stores all the mogador gem installable and usable Ruby templates.',
      name: 'mogador-templates',
      name_with_namespace: 'Guillaume Hain / mogador-templates',
      path: 'mogador-templates',
      path_with_namespace: 'zedtux/mogador-templates',
      created_at: '2020-10-27T20:55:27.469Z',
      default_branch: 'master',
      tag_list: [],
      ssh_url_to_repo: 'git@gitlab.com:zedtux/mogador-templates.git',
      http_url_to_repo: 'https://gitlab.com/zedtux/mogador-templates.git',
      web_url: 'https://gitlab.com/zedtux/mogador-templates',
      readme_url: 'https://gitlab.com/zedtux/mogador-templates/-/blob/master/README.md',
      avatar_url: nil,
      forks_count: 0,
      star_count: 0,
      last_activity_at: '2020-10-27T20:55:27.469Z',
      namespace: {
        id: '122100',
        name: 'Guillaume Hain',
        path: 'zedtux',
        kind: 'user',
        full_path: 'zedtux',
        parent_id: nil,
        avatar_url: 'https://secure.gravatar.com/avatar/609b69cbb09257883a4e32d7d5aff098?s=80&d=identicon',
        web_url: 'https://gitlab.com/zedtux'
      }
    }.to_json
  end

  def gitlab_mogador_templates_project_tree_json
    [
      {
        id: '1a8f6bce4c619dcef861cbb2265b2a77b70ec5ad',
        name: 'rails-5-base',
        type: 'tree',
        path: 'rails-5-base',
        mode: '040000'
      }, {
        id: '46f2994fdda198e9c048e94909db159a289b778a',
        name: 'rails-6-base',
        type: 'tree',
        path: 'rails-6-base',
        mode: '040000'
      }, {
        id: '2b57079083fabdaca6c33b95e973d3f041ab55d8',
        name: 'README.md',
        type: 'blob',
        path: 'README.md',
        mode: '100644'
      }
    ].to_json
  end

  def not_found_json
    {}.to_json
  end
end
