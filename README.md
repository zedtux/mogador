# Mogador

This gem allows you to segregate your app code (controllers, models, views, jobs and so on) from your framework's folder structure.

For example, with a Rails application, with Mogador you will have a folder with a Rails empty app, and a folder with your code, and Mogador glue them together.

Mogador is what RMV or rbenv are for Ruby, but for your framework!

# Project status

The basics are implemented, and it is possible to use to switch from a template to another, but there couple of things to be fixed before to see any kind of potential attraction to the project:

* A `Gemfile` is generated in a `/tmp/<generated string/` folder each time the `mogador exec` command is used (missing cleaning or should use one folder)
* I have a case where running `mogador exec rails -s` doesn't started Rails, and exit the command without any output, with an exit code 0.
* The `mogador templates use` command generate a `.mogador.yml` file storing the template path, instead of the template name. Then the command needs to look at the templates catalog in order to find back the template path
* The mogador gem needs a command to create new projects

For now, I'm stopping developing it, will see in the future if I come back on it or if I just delete it.

## Benefits

* *Clean code base:* You only work with your files.
* *Templates:* Now that your framework leaves in another folder, you can switch between different one.

## Templates

[Mogador templates](https://gitlab.com/zedtux/mogador-templates) are empty framework's app (like empty Rails app you have after having ran `rails new ...`) with a Mogador integration added.

This integration is Ruby code which glue your code to the framework.

As of now, Mogador has 2 templates:

* Rails 5 ([Template source](https://gitlab.com/zedtux/mogador-templates/-/tree/master/rails-5-base))
* Rails 6 ([Template source](https://gitlab.com/zedtux/mogador-templates/-/tree/master/rails-6-base))

This list will grow with time and a dedicated page about how to add templates will be written soon.

## Installation

For now, Mogador runs in an existing app but will also take care of project creation later.

1. In your project, add the following line to your `Gemfile`:
```ruby
gem 'mogador', '~> 0.1'
```
2. And remove all the gems from your framework like the `rails` gem.
3. Run `bundle install`
4. Download/Install a Mogador template (See the [Usage](#usage))

## Usage

```
# 1. Check the available templates
bundle exec mogador templates list

# 2. Decide to install the rails-5-base template
bundle exec mogador templates install rails-5-base

# 3. Tells Mogador to use the template in your project
cd /to/your/project/folder
bundle exec mogador templates use rails-5-base

# 4. Start the web server
bundle exec mogador exec rails s -b 0.0.0.0'

# 5. Run the migration scripts (if any)
bundle exec mogador exec rails db:migrate
```

Now you should be able to reach your application.

### `exec` 

The `mogador exec` command allows you to run your commands into the project's template, using the Mogador integration.

Here are some examples of its usage:

* Create a new migration script:
```
bundle exec mogador exec rails g migration MigrationNameHere
```
* Run migration scripts:
```
bundle exec mogador exec rails db:migrate
```
* See the available routes in your Rails application:
```
bundle exec mogador exec rails routes
```
* Access a Rails console:
```
bundle exec mogador exec rails c
```
* Running the Rails server:
```
bundle exec mogador exec rails s
```

And so on.

### `templates list`

Lists all the available templates from [the Mogador templates repository](https://gitlab.com/zedtux/mogador-templates).

```
$ bundle exec mogador templates list
Available templates:

  rails-5-base
  rails-6-base
  

Use mogador install <template name> in order to install it.
```

### `templates install <name>`

Downloads and extracts the given template name into the Mogador's templates cache folder.

```
$ bundle exec mogador templates install rails-5-base
Installating rails-5-base ...
Downloading template archive to /mogador/templates_cache ...
Downloading https://gitlab.com/zedtux/mogador-templates/-/archive/master/mogador-templates-master.tar.gz?path=rails-5-base into /mogador/templates_cache/rails-5-base.tar.gz ...
SUCCESS: Mogador template rails-5-base successfully installed.
```

### `templates use <name>`

Creates a `.mogador.yml` file setting a `template_path` as the template's installation path.

This file is then used by the `mogador exec <cmd>` command in order to execute it in the template's folder.

```
$ mogador templates use rails-5-base
```
