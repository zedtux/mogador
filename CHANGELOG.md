# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Initial release
- Adds the `mogador version` command
- Adds the `mogador exec` command
- Adds the `mogador templates list` command
- Adds the `mogador templates install` command
- Adds the `mogador templates use` command

[Unreleased]: https://gitlab.com/pharmony/sauron/-/commits/master
