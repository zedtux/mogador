# frozen_string_literal: true

require 'gitlab'
require 'thor'

require_relative 'commands/exec'
require_relative 'commands/templates/common'
require_relative 'commands/templates/install'
require_relative 'commands/templates/list'
require_relative 'commands/templates/use'
require_relative 'commands/version'

require_relative 'configuration'
require_relative 'errors'
require_relative 'gemfile'

Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = ''
end

module Mogador
  #
  # Templates management sub commands
  #
  class Templates < Thor
    desc 'install TEMPLATE_NAME', 'Installs the given template name'
    def install(name)
      Mogador::Commands::Templates::Install.run(name)
    end

    desc 'list', 'List all the available templates'
    def list
      Mogador::Commands::Templates::List.run
    end

    desc 'use TEMPLATE_NAME', 'Creates/updates the .mogador.yml file to use ' \
                              'the given TEMPLATE_NAME'
    def use(name)
      Mogador::Commands::Templates::Use.run(name)
    end
  end

  #
  # Root CLI commands
  #
  class Cli < Thor
    desc 'exec COMMAND', 'Execute the given command in the base framework'
    def exec(*args)
      Mogador::Commands::Exec.run(*args)
    end

    desc 'version', 'Show the version and exist'
    def version
      Mogador::Commands::Version.run
    end

    desc 'templates SUBCOMMAND ...ARGS', 'Manage the Mogador templates'
    subcommand 'templates', Templates

    no_commands do
      def exit_on_failure?
        true
      end
    end
  end
end

Mogador::Cli.start(ARGV)
