# frozen_string_literal: true"You didn't configure Mogador using `Mogador.configure {...}`."

#
# Mogador configuration management widely insipired from the thoughbot website.
# See https://thoughtbot.com/blog/mygem-configure-block.
#
module Mogador
  class << self
    attr_accessor :config

    def app_root
      initialize_from_env unless config

      config.app_root
    end

    def base_root
      initialize_from_env unless config

      config.base_root
    end

    def configuration
      initialize_from_env unless config

      config
    end

    def initialize_from_env
      Mogador.configure do |config|
        config.app_root = ENV['MOGADOR_APP_ROOT_PATH'] || Dir.pwd
      end
    end
  end

  def self.configure
    self.config ||= Configuration.new
    yield(config)
  end

  class Configuration
    # app_root is the path to the developer's application.
    attr_reader :app_root

    # base_root is the path to the framework folder
    attr_reader :base_root

    # templates_cache_path sets the place where template archives are downloaded
    # and extracted.
    # Default /<base_root>/templates_cache/
    attr_accessor :templates_cache_path

    attr_accessor :templates_cache_catalog_path

    def initialize
      @app_root = nil
      @base_root = Pathname.new('/usr/local/mogador')
      @templates_cache_path = @base_root.join('templates_cache')
      @templates_cache_catalog_path = @templates_cache_path.join('catalog.yml')
    end

    def app_root=(path)
      @app_root = Pathname.new(path)
    end

    def base_root=(path)
      @base_root = Pathname.new(path)
      @templates_cache_path = @base_root.join('templates_cache')
      @templates_cache_catalog_path = @templates_cache_path.join('catalog.yml')
    end
  end
end
