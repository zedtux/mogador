# frozen_string_literal: true

module Mogador
  module Errors
    class NoInstalledTemplateError < StandardError; end
    class NotInitializedError < StandardError; end
    class TemplateFetchingError < StandardError; end
    class TemplateInstallError < StandardError; end
  end
end
