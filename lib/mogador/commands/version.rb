# frozen_string_literal: true

module Mogador
  module Commands
    #
    # Mogador version command line
    #
    module Version
      def self.run
        puts "v#{Mogador::VERSION}"
      end
    end
  end
end
