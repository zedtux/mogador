# frozen_string_literal: true

module Mogador
  module Commands
    #
    # Shared methods across all templates sub commands
    #
    module Common
      def mogador_templates_project
        @mogador_templates_project ||= Gitlab.project('zedtux/mogador-templates')
      rescue Gitlab::Error::NotFound
        raise Mogador::Errors::TemplateFetchingError,
              'Something went wrong, mogador-templates project cannot be found'
      end

      def mogador_templates_names
        tree = Gitlab.tree(mogador_templates_project.id)

        tree.map do |page|
          next unless page.type == 'tree'

          page.name
        end
      rescue Gitlab::Error::NotFound
        raise Mogador::Errors::TemplateFetchingError,
              'The mogador-templates project has no templates yet'
      end
    end
  end
end
