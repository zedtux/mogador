# frozen_string_literal: true

module Mogador
  module Commands
    module Templates
      #
      # Mogador templates use command line creates/updates the .mogador.yml file
      # from the current folder in order to set the `template_name` setting.
      # This setting is later used by the `exec` command in order to run
      # the given command into the right template folder.
      #
      module Use
        extend Common

        def self.run(name)
          ensure_template_is_installed!(name)

          write_mogador_yml_file(name)
        rescue Mogador::Errors::NoInstalledTemplateError
          puts "ERROR: There's no installed templates yet."
          puts
          puts 'Please use the `mogador templates list` command to see the ' \
               'available templates, and then use the `mogador templates ' \
               'install` command to install one of them.'
          puts 'Then you can run the `mogador templates use` command again.'

          raise
        end

        def self.ensure_catalog_includes_template!(name)
          catalog = templates_cache_catalog

          return if catalog['templates'].key?(name)

          raise Mogador::Errors::NoInstalledTemplateError,
                "ERROR: There's no installed templates yet."
        end

        def self.ensure_templates_cache_catalog_exist!
          File.exist?(Mogador.configuration.templates_cache_catalog_path) && \
            return

          raise Mogador::Errors::NoInstalledTemplateError,
                "ERROR: There's no installed templates yet."
        end

        def self.ensure_templates_cache_folder_exist!
          return if File.exist?(Mogador.configuration.templates_cache_path)

          raise Mogador::Errors::NoInstalledTemplateError,
                "ERROR: There's no installed templates yet."
        end

        def self.ensure_templates_cache_folder_is_configured!
          return if Mogador.configuration.templates_cache_path

          raise Mogador::Errors::NoInstalledTemplateError
        end

        def self.ensure_template_is_installed!(name)
          ensure_templates_cache_folder_is_configured!
          ensure_templates_cache_folder_exist!
          ensure_templates_cache_catalog_exist!
          ensure_catalog_includes_template!(name)
        end

        def self.templates_cache_catalog(reset = false)
          @templates_cache_catalog = nil if reset

          @templates_cache_catalog ||= YAML.load_file(
            Mogador.configuration.templates_cache_catalog_path
          )
        end

        def self.write_mogador_yml_file(name)
          catalog = templates_cache_catalog

          config = {}
          config['template_path'] = catalog['templates'][name]

          File.open(File.join(Dir.pwd, '.mogador.yml'), 'w') do |file|
            file.write config.to_yaml
          end
        end
      end
    end
  end
end
