# frozen_string_literal: true

module Mogador
  module Commands
    module Templates
      #
      # Mogador templates list command line shows the available template names
      # fetched from the Mogador's templates repository.
      #
      module List
        extend Common

        def self.run
          # Fetches the project from Gitlab, or raise an error
          mogador_templates_project

          # Fetches templates from Gitlab project or raise an error
          names = mogador_templates_names

          puts "Available templates:\n\n"
          names.each do |name|
            puts "  #{name}"
          end
          puts
          puts 'Use mogador install <template name> in order to install it.'
        end
      end
    end
  end
end
