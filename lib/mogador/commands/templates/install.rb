# frozen_string_literal: true

require 'rubygems/package'

module Mogador
  module Commands
    module Templates
      #
      # Mogador templates install command line download/extract a template into
      # the Mogador's templates cache folder.
      #
      module Install
        extend Common

        def self.run(name)
          puts "Installating #{name} ..."

          ensure_template_name_exists!(name)

          template_archive_path = downlod_template(
            name,
            to: Mogador.configuration.templates_cache_path
          )

          template_path = extract_template_archive(template_archive_path)

          register_template_to_catalog!(name, template_path)

          puts "SUCCESS: Mogador template #{name} successfully installed."
        end

        def self.build_template_url_for_template_name(name)
          # base_url = https://gitlab.com/zedtux/mogador-templates/
          base_url = mogador_templates_project.web_url
          # download_url = https://gitlab.com/zedtux/mogador-templates/-/archive/master/mogador-templates-master.tar.gz
          # TODO : Manage versions and change the `master` branch here with a tag
          download_url = "#{base_url}/-/archive/master/mogador-templates-master.tar.gz"

          URI("#{download_url}?path=#{name}")
        end

        # TODO : Extract the download methods
        def self.downlod_template(name, options = {})
          raise ArgumentError, '"to" option is missing' unless options.key?(:to)

          template_url = build_template_url_for_template_name(name)

          puts "Downloading template archive to #{options[:to]} ..."
          FileUtils.mkdir_p(options[:to])
          destination_path = File.join(options[:to], "#{name}.tar.gz")

          download_url_to_file(template_url, destination_path)

          destination_path
        end

        def self.download_url_to_file(url, path)
          puts "Downloading #{url} into #{path} ..."
          File.open(path, 'wb') do |destination_file|
            Net::HTTP.start(url.host, url.port, use_ssl: true) do |http|
              request = Net::HTTP::Get.new(url.request_uri)
              http.request(request) do |response|
                destination_file.write(response.body)
              end
            end
          end
        end

        def self.ensure_template_name_exists!(name)
          return if mogador_templates_names.include?(name)

          raise Mogador::Errors::TemplateInstallError,
                "The template #{name.inspect} doesn't exist"
        end

        #
        # This method extracts the given archive, in the same folder and rebuild
        # the archive folder structure ignoring the archive's root folder as it
        # is something like `mogador-templates-master-rails-5-base` for
        # the `rails-5-base` template.
        #
        # TODO : Extract the extract methods
        def self.extract_template_archive(from_path)
          unless File.exist?(from_path)
            raise TemplateFileNotFound,
                  "The template path #{from_path} doesn't exist."
          end

          parent_folder_path = File.dirname(from_path)

          template_path = nil

          File.open(from_path, 'rb') do |file|
            Zlib::GzipReader.wrap(file) do |gz|
              Gem::Package::TarReader.new(gz) do |tar|
                tar.each do |entry|
                  next unless entry.file?

                  file_dirname = File.join(
                    parent_folder_path,
                    File.dirname(entry.full_name).gsub(%r{^[^\/]*}, '')
                  )
                  file_basename = File.basename(entry.full_name)

                  FileUtils.mkdir_p(file_dirname)
                  template_path ||= file_dirname

                  real_output_path = File.join(file_dirname, file_basename)

                  File.open(real_output_path, 'wb') do |f|
                    f.write(entry.read)
                  end

                  File.chmod(entry.header.mode, real_output_path)
                end
              end
            end
          end

          template_path
        end

        def self.load_templates_cache_catalog
          YAML.load_file(
            Mogador.configuration.templates_cache_catalog_path
          )
        rescue Errno::ENOENT
          {}
        end

        def self.write_templates_cache_catalog(catalog)
          File.open(
            Mogador.configuration.templates_cache_catalog_path,
            'w'
          ) do |file|
            file.write catalog.to_yaml
          end
        end

        def self.register_template_to_catalog!(name, template_path)
          catalog = load_templates_cache_catalog

          catalog['templates'] ||= {}
          catalog['templates'][name] = template_path

          write_templates_cache_catalog(catalog)
        end
      end
    end
  end
end
