# frozen_string_literal: true

module Mogador
  module Commands
    #
    # Mogador exec command line
    #
    module Exec
      def self.run(*args)
        # Removes the `exec` from the arguments
        ARGV.shift

        if args.empty?
          puts 'ERROR: You must pass a command to be executed.'

          # TODO : Find a way to avoid `exit 1` when running the tests
          exit 1 unless ENV['RUBY_ENV'] == 'test'
        else
          if ensure_mogador_yml_file_is_present!
            configure_mogador_using_mogador_yml_file

            Dir.chdir(Mogador.base_root) do
              # Saves the Mogador's app_root in an environment variable so that it
              # survives call `system(cmd)`
              ENV['MOGADOR_APP_ROOT_PATH'] = Mogador.app_root.to_s

              # Create a Gemfile including both app_root's Gemfile
              # base_root Gemfile.
              ENV['BUNDLE_GEMFILE'] = Mogador::Gemfile.combined_gemfiles_path

              STDOUT.sync = true

              # Runs the passed command
              system(args.join(' '))
            end
          else
            # TODO : Find a way to avoid `exit 1` when running the tests
            exit 1 unless ENV['RUBY_ENV'] == 'test'
          end
        end
      end

      def self.configure_mogador_using_mogador_yml_file
        mogador_config = YAML.load_file(mogador_yml_path)

        Mogador.configure do |config|
          config.app_root = Dir.pwd
          config.base_root = mogador_config['template_path']
        end
      end

      def self.ensure_mogador_yml_file_is_present!
        return true if File.exist?(mogador_yml_path)

        puts 'ERROR: Mogador not configured, please use `mogador templates ' \
             'use` command.'

        false
      end

      def self.mogador_yml_path
        File.join(Dir.pwd, '.mogador.yml')
      end
    end
  end
end
