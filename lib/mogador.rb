# frozen_string_literal: true

require 'gitlab'

require 'mogador/configuration'
require 'mogador/version'

module Mogador
end
