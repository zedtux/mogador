# frozen_string_literal: true

require_relative 'lib/mogador/version'

gitlab_base = 'https://gitlab.com/zedtux/mogador'

Gem::Specification.new do |spec|
  spec.name          = 'mogador'
  spec.version       = Mogador::VERSION
  spec.authors       = ['Guillaume Hain']
  spec.email         = ['zedtux@zedroot.org']

  spec.summary       = 'Decouple your app from your framework.'
  spec.description   = File.read('README.md').scan(
    /\A#.*$\n\n([\w\W]+)\n\n##/
  ).flatten.first

  spec.homepage      = gitlab_base
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata = {
    'homepage_uri' => spec.homepage,
    'source_code_uri' => gitlab_base,
    'changelog_uri' => gitlab_base + '/-/blob/master/CHANGELOG.md'
  }

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added
  # into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |file|
      file.match(%r{^(test|spec|features)/|^Earthfile$})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |file| File.basename(file) }
  spec.require_paths = ['lib']

  spec.add_dependency 'gitlab', '~> 4.16'
  spec.add_dependency 'thor', '~> 1.0'
end
